<?php                                                                           

// development                                                                         
$db = new mysqli("localhost", "root", "ali", "phpfox");
$db->set_charset('utf8');                                                       
                                                                                
// test kullanıcılar bunun dbden geldiğini varsayıyorum.                        
$users = array(                                                                 
    array('name' => 'Ali OYGUR 1', 'email' => 'alioygur1@gmail.com', 'password' => '6644801'),
    array('name' => 'Ali OYGUR 2', 'email' => 'alioygur2@gmail.com', 'password' => '123456'),
    array('name' => 'Ali OYGUR 3', 'email' => 'alioygur3@gmail.com', 'password' => '12345')
);                                                                              
                                                                                
function generate_salt($iTotal = 3)                                             
{                                                                               
    $sSalt = '';                                                            
    for ($i = 0; $i < $iTotal; $i++)                                        
    {                                                                       
            $sSalt .= chr(rand(33, 91));                                    
    }                                                                       
    return $sSalt;                                                          
}                                                                               
                                                                                
foreach ($users as $user) {                                                     
                                                                                
    $salt = generate_salt();                                                
                                                                            
    $row = array(                                                           
        'user_id' => NULL,                                              
        'user_group_id' => 2, // registered users                       
        'full_name' => $user['name'],                                   
        'joined' => time(),                                             
        'password' => md5(md5($user['password']) . md5($salt)),         
        'password_salt' => $salt,                                       
        'email' => $user['email']                                       
    );                                                                      
                                                                            
    $q = $db->prepare("INSERT INTO phpfox_user(user_id, user_group_id, full_name, joined, password, password_salt, email) VALUES(?,?,?,?,?,?,?);") or die($db->error);
    $q->bind_param('iisisss', $row['user_id'],$row['user_group_id'],$row['full_name'],$row['joined'],$row['password'],$row['password_salt'], $row['email']);
                                                                            
    if($q->execute()) {

        $user_id = $db->insert_id;                                      
                                                                        
        $db->query("INSERT INTO phpfox_user_activity(user_id) VALUES({$user_id})");
        $db->query("INSERT INTO phpfox_user_field(user_id) VALUES({$user_id})");
        $db->query("INSERT INTO phpfox_user_space(user_id) VALUES({$user_id})");
        $db->query("INSERT INTO phpfox_user_count(user_id) VALUES({$user_id})");

        // set usernames
        $db->query("UPDATE phpfox_user SET user_name = CONCAT('profile-', user_id)");
                                                                            
    } else {                                                                
    	echo $user['email']." | FALSE | ".$db->error.PHP_EOL;           
    }                                                                       
}  
