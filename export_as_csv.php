<?php
/**
 * DB deki türkçe karekter problemlerinden dolayı. CSV çıktıları bu şekilde alınıyor
 */
define('PHPFOX', true);
define('PHPFOX_DS', DIRECTORY_SEPARATOR);
define('PHPFOX_DIR', dirname(__FILE__) . PHPFOX_DS);
define('PHPFOX_START_TIME', array_sum(explode(' ', microtime())));
require_once(PHPFOX_DIR . 'include' . PHPFOX_DS . 'init.inc.php');

header( 'Content-Type: text/html; charset=utf8' ); // tell the browser to treat file as CSV

if(!isset($_GET['token']) && $_GET['token'] != '20045070') exit(null);
if(!isset($_GET['table'])) exit('tablo seçilmedi');
if(!isset($_GET['columns'])) exit('kolun(lar) seçilmedi');

$table = $_GET['table'];
$select = $_GET['columns'];
$result = Phpfox::getLib('database')
    ->select($select)
    ->from($table)
    ->execute('getRows');

echo implode(',', array_keys($result[0])) . "<br />";
foreach ($result as $row)
{
    $_row = array();
    foreach ($row as $key => $value)
    {
        $_row[] = "\"" . trim($value) . "\"";
    }

    $line = implode(",", $_row);

    echo $line . "<br />";
}
