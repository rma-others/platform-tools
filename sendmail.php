<?php 

// dev
$db = new mysqli("localhost", "root", "ali", "test");

// production
//$db = new mysqli("localhost", "?", "?", "?");

$db->set_charset('utf8');  

require_once('phpmailler/class.phpmailer.php');
//include("class.smtp.php"); // optional, gets called from within class.phpmailer.php if not already loaded

$mail                = new PHPMailer();

/*
$mail->IsSMTP(); // telling the class to use SMTP
$mail->Host          = "smtp1.site.com;smtp2.site.com";
$mail->SMTPAuth      = true;                  // enable SMTP authentication
$mail->SMTPKeepAlive = true;                  // SMTP connection will not close after each email sent
$mail->Host          = "mail.yourdomain.com"; // sets the SMTP server
$mail->Port          = 26;                    // set the SMTP port for the GMAIL server
$mail->Username      = "yourname@yourdomain"; // SMTP account username
$mail->Password      = "yourpassword";        // SMTP account password
*/

$mail->SetFrom('list@mydomain.com', 'List manager');
$mail->AddReplyTo('list@mydomain.com', 'List manager');
$mail->Subject = "Mail Konusu";

$q = $db->query("SELECT * FROM `users` WHERE `is_send` = '0' LIMIT 2");

if($q->num_rows)
{
  while ($row = $q->fetch_object()) {
    
    $body  = "<h1>Merhaba, {$row->name}</h1>";
    $body .= "<p>Lorem ipsum dolar sit amet<p>";
    $body .= "<p>Katılmak için <a href=\"http://dev1.rmaydin.com/survey/registration/{$row->user_id}/$row->user_token\">Tıklayın</a></p>";

    $mail->MsgHTML($body);
    $mail->AddAddress($row->email, $row->name);

    if(!$mail->Send()) {

      $error = $db->real_escape_string($mail->ErrorInfo);
      $db->query("UPDATE users SET `error` = '$error' WHERE `user_id` = {$row->user_id}");

      echo "Hata! " . $mail->ErrorInfo.PHP_EOL;

    } else {

      echo "Okey".PHP_EOL;
      $db->query("UPDATE users SET `is_send` = '1' WHERE `user_id` = {$row->user_id}");
      
    }

    // Clear all addresses and attachments for next loop
    $mail->ClearAddresses();  
  }

}