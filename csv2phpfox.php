<?php
/**                                                                             
 * Kullanıcıları csv dosyasından phpfox user tablosuna aktarır.
 *
 * DIKKAT! DIREKT OLARAK KULLANILMAMALIDIR. 
 * okunacak olan csv dosyasındaki sütün sayısı burası ile karşılaştırılmalı
 * sütünda yer alan field lar ile buradakiler karşılaştırılmalı
 * csv dosyasında başlık olmamalıdır.
 *
 * @since  2013-02-01
 * @author Ali OYGUR <alioygur@gmail.com>
 */                                                                                                                    

$file = '../user_Destek_platformu_Eklenecek_Listesi_20130509.csv';                                                            
                                                                                
// dev                                                                          
//$db = new mysqli("localhost", "root", "ali", "phpfox");                       
                                                                                
// production                                                                   
$db = new mysqli("localhost", "root", "aws5399QAR1845tgbc","phpfox_ailehek");

//$db->set_charset('utf8');                                                     
                                                                                
$data = array();                                                                
                                                                                
$handle = fopen($file, "r");                                                    
                                                                                
$i = 0;
                                                           
while(($_row = fgetcsv($handle, 1024, ",")) !== FALSE){                                                                               
    $i++;                                                                       
                                                                                
    if(count($_row) != 8){                                                      
        echo $i . ". | FALSE | Satır hatalı (Tam olarak 8 sütundan oluşmuyor)".PHP_EOL;
        continue;                                                               
    }                                                                           
    list(                                                                       
        $full_name,                                                             
        $email,                                                                 
        $city,                                                                  
        $workplace,
        $mobile,                                                              
        $gender,
        $birthday,                                                                
        $password                                                               
        ) = $_row;                                                              


$full_name=$_row['2']."".$_row['3'];
$password=$_row['6'];
$email=$_row['4'];
                                                                                
    $salt = generate_salt();                                                    
                                                                                
    $row = array(                                                               
        'user_id' => NULL,                                                      
        'user_group_id' => 2, // registered users                               
        'full_name' => $full_name,                                              
        'joined' => time(),                                                     
        'password' => md5(md5($password) . md5($salt)),                         
        'password_salt' => $salt,                                               
        'email' => $email,                                                      
        'country_iso' => 'TR',                                                  
        'gender' =>  _gender($gender),                                          
        'birthday' => _birthday($birthday)                                    
    );                                                                          

                                                                                
    $q = $db->prepare("INSERT INTO phpfox_user(user_id, user_group_id, full_name, joined, password, password_salt, email, country_iso, gender, birthday) VALUES(?,?,?,?,?,?,?,?,?,?);");
    $q->bind_param('iisissssis', $row['user_id'],$row['user_group_id'],$row['full_name'],$row['joined'],$row['password'],$row['password_salt'], $row['email'], $row['country_iso'], $row['gender'], $row['birthday']);
                                                                                
    if($q->execute()) {                                                         
                                                                                
        $user_id = $db->insert_id;                                              
                                                                                
        $db->query("INSERT INTO phpfox_user_activity(user_id) VALUES({$user_id})");
        $db->query("INSERT INTO phpfox_user_field(user_id, city_location) VALUES({$user_id}, '{$city}')");
        $db->query("INSERT INTO phpfox_user_custom(user_id, cf_telefon, cf_al_t_yer) VALUES({$user_id}, '{$mobile}', '{$workplace}')");
        $db->query("INSERT INTO phpfox_user_space(user_id) VALUES({$user_id})");
        $db->query("INSERT INTO phpfox_user_count(user_id) VALUES({$user_id})");
                                                                                
                                                                                
        // set usernames                                                        
        $db->query("UPDATE phpfox_user SET user_name = CONCAT('profile-', user_id) WHERE user_id = {$user_id}");
                                                                                
        echo "{$full_name} | Kullanıcı Oluşturuldu.".PHP_EOL;                   
                                                                                
    } else{                                                                     
        echo "{$full_name} | Hata! kullanıcı oluşturulamadı.".PHP_EOL;          
    }                                                                           
}                                                                               
                                                                                
function generate_salt($iTotal = 3)                                             
{                                                                               
    $sSalt = '';                                                                
    for ($i = 0; $i < $iTotal; $i++)                                            
    {                                                                           
            $sSalt .= chr(rand(33, 91));                                        
    }                                                                           
    return $sSalt;                                                              
}                                                                               
                                                                                
function _birthday($birthday)                                                   
{                                                                               
    if(empty($birthday)) return NULL;                                           
                                                                                
    $time = strtotime(str_replace('/', '-', $birthday));                        
                                                                                
    return date('mdY', $time);                                                  
}                                                                               
                                                                                
function _gender($gender)                                                       
{                                                                               
    if($gender == 'Bay') return 1;                                              
    if($gender == 'Bayan') return 2;                                            
                                                                                
    return 0;                                                                   
}         
